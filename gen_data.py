import torch.utils.data as data
from PIL import Image
from torchvision.transforms import Compose,ToTensor, Normalize
import os


class GenData(data.Dataset):
    def __init__(self, crop_size, gen_data_dir):
        super().__init__()
        haze_names = []
        for i in os.listdir(gen_data_dir):
            temp = i
            haze_names.append(temp)
        self.haze_names = haze_names
        self.gen_data_dir = gen_data_dir
        self.crop_size = crop_size

    def get_images(self, index):
        crop_width, crop_height = self.crop_size
        haze_name = self.haze_names[index]

        haze_img = Image.open(self.gen_data_dir+ '/' + haze_name).convert('RGB')

        width, height = haze_img.size
        if width < crop_width or height < crop_height:
            raise Exception('Bad image size:{}'.format(haze_name))

        x, y = width/2, height/2
        haze_crop_img = haze_img.crop((x-crop_width/2, y-crop_height/2, x+crop_width/2, y+crop_height/2))

        transform_haze = Compose([ToTensor()])
        # transform_haze = Compose([ToTensor(), Normalize((0.5, 0.5, 0.5),(0.5, 0.5, 0.5))])
        haze = transform_haze(haze_crop_img)

        if list(haze.shape)[0] is not 3:
            raise Exception('Bad image channel: {}'.format(haze_name))

        return haze, haze_name


    def __getitem__(self, index):
        res= self.get_images(index)
        return  res


    def __len__(self):
        return len(self.haze_names)


# real_data = './real_haze'
# # # haze, haze_name = GenData([240, 240], real_data)
# # # print(haze)
# # # print('1', haze_name)
# haze_names=[]
# for i in os.listdir(real_data):
#     temp = i
#     haze_names.append(temp)
# print(haze_names)
# h1 = haze_names[0]
# print(h1)