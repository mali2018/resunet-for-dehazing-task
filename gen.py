import time
import torch
import argparse
import torch.nn as nn
from torch.utils.data import DataLoader
from gen_data import GenData
from model import *
import torchvision.utils as utils


def save_images(net, gen_data_loader, device):
    for batch_id, gen_data in enumerate(gen_data_loader):
        with torch.no_grad():
            haze, image_name = gen_data
            haze = haze.to(device)
            dehaze = net(haze)
        save_f(dehaze, haze,image_name)

def save_f(dehaze, haze, image_name):
    haze_images = torch.split(haze, 1, dim=0)
    dehaze_images = torch.split(dehaze, 1, dim=0)
    batch_num = len(dehaze_images)
    haze_save_path = './real_dehaze/haze'
    dehaze_save_path = './real_dehaze/dehaze'
    if not os.path.exists(haze_save_path):
        os.makedirs((haze_save_path))
    if not os.path.exists(dehaze_save_path):
        os.makedirs((dehaze_save_path))
    for ind in range(batch_num):
        utils.save_image(dehaze_images[ind],dehaze_save_path+'/{}'.format(image_name[ind][:-3]+'png'))
        utils.save_image(haze_images[ind], haze_save_path+'/{}'.format(image_name[ind][:-3]+'png'))


parser = argparse.ArgumentParser(description='Hyper-parameters for ResUNet')

parser.add_argument('--lambda_loss', help='Set the lambda in loss function', default=0.04, type=float)
parser.add_argument('--gen_batch_size', help='Set the validation/test batch size', default=1, type=int)
# parser.add_argument('-category', help='Set image category (indoor or outdoor?)', default='indoor', type=str)
parser.add_argument('--crop_size', help='Set the crop_size', default=[240, 240], nargs='+', type=int)
parser.add_argument('--model_dir', help='directory wherer save model paameter', default='dec_save', type=str)
args = parser.parse_args()

lambda_loss = args.lambda_loss
gen_batch_size = args.gen_batch_size
# category = args.category
crop_size = args.crop_size
model_dir = args.model_dir

print('-- Hyper-parameters for testing --')
print('gen_batch_size:{}\nlambda_loss:{}'.format(gen_batch_size, lambda_loss))

gen_data_dir = './real_haze'

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
gen_data_loader = DataLoader(GenData(crop_size, gen_data_dir), batch_size=gen_batch_size, shuffle=False, num_workers=4)
net = ResUnet()

net = net.to(device)

net.dec.load_state_dict(torch.load('{}'.format(model_dir)))

net.eval()
start_time = time.time()

save_images(net, gen_data_loader, device)
end_time = time.time() - start_time

print('validation time is {0:.4f}'.format(end_time))


