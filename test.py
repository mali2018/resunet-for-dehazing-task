import time
import torch
import argparse
import torch.nn as nn
from torch.utils.data import DataLoader
from val_data import ValData
from model import *
from utils import validation


parser = argparse.ArgumentParser(description='Hyper-parameters for ResUNet')
parser.add_argument('-lambda_loss', help='Set the lambda in loss function', default=0.04, type=float)
parser.add_argument('-val_batch_size', help='Set the validation/test batch size', default=1, type=int)
parser.add_argument('-category', help='Set image category (indoor or outdoor?)', default='indoor', type=str)
parser.add_argument('--crop_size', help='Set the crop_size', default=[240, 240], nargs='+', type=int)
parser.add_argument('--model_dir', help='directory wherer save model paameter', default='dec_save', type=str)
args = parser.parse_args()

lambda_loss = args.lambda_loss
val_batch_size = args.val_batch_size
category = args.category
crop_size = args.crop_size
model_dir = args.model_dir

print('-- Hyper-parameters for testing --')
print('val_batch_size: {}\nlambda_loss: {}\ncategory: {}'.format(val_batch_size, lambda_loss, category))


if category == 'indoor':
    val_data_dir = '/home/chen009/Desktop/ResUnetv0.2/data/test/SOTS/indoor/'
elif category == 'outdoor':
    val_data_dir = './data/test/SOTS/outdoor/'
# real world
elif category == 'real_world':
    val_data_dir = '/home/chen009/Desktop/ResUnetv0.2/data/test/SOTS/outdoor/'
else:
    raise Exception('Wrong image category. Set it to indoor or outdoor for RESIDE dateset.')


device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
val_data_loader = DataLoader(ValData(crop_size, val_data_dir), batch_size=val_batch_size, shuffle=False, num_workers=4)
net = ResUnet()

net = net.to(device)


# load the network weight
net.dec.load_state_dict(torch.load('{}'.format(model_dir)))

net.eval()
start_time = time.time()
val_psnr, val_ssim = validation(net, val_data_loader, device, category, save_tag=True)
end_time = time.time() - start_time
print('val_psnr: {0:.2f}, val_ssim: {1:.4f}'.format(val_psnr, val_ssim))
print('validation time is {0:.4f}'.format(end_time))