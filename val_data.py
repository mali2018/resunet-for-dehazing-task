import torch.utils.data as data
from PIL import Image
from torchvision.transforms import Compose, ToTensor, Normalize
from random import randrange
import torch
from utils import to_psnr, to_ssim_skimage
import os
import time

# --- Validation/test dataset --- #
class ValData(data.Dataset):
    def __init__(self, crop_size, val_data_dir):
        super().__init__()
        val_list = val_data_dir + 'val_list.txt'
        with open(val_list) as f:
            contents = f.readlines()
            haze_names = [i.strip() for i in contents]
            gt_names = [i.split('_')[0] + '.png' for i in haze_names]

        self.haze_names = haze_names
        self.gt_names = gt_names
        self.val_data_dir = val_data_dir
        self.crop_size = crop_size

    def get_images(self, index):
        crop_width, crop_height = self.crop_size
        haze_name = self.haze_names[index]
        gt_name = self.gt_names[index]
        haze_img = Image.open(self.val_data_dir + 'hazy/' + haze_name)
        gt_img = Image.open(self.val_data_dir + 'clear/' + gt_name)

        width, height = haze_img.size

        if width < crop_width or height < crop_height:
            raise Exception('Bad image size: {}'.format(gt_name))

        x, y = width / 2, height / 2
        haze_crop_img = haze_img.crop((x - crop_width / 2, y - crop_height / 2, x + crop_width / 2, y + crop_height / 2))
        gt_crop_img = gt_img.crop((x - crop_width / 2, y - crop_height / 2, x + crop_width / 2, y + crop_height / 2))
        # haze_crop_img = haze_img.crop((0, 0, crop_width, crop_height))
        # gt_crop_img = gt_img.crop((0, 0, crop_width, crop_height))

        # # --- x,y coordinate of left-top corner --- #
        # x, y = randrange(0, width - crop_width + 1), randrange(0, height - crop_height + 1)
        # haze_crop_img = haze_img.crop((x, y, x + crop_width, y + crop_height))
        # gt_crop_img = gt_img.crop((x, y, x + crop_width, y + crop_height))

        # --- Transform to tensor --- #
        transform_haze = Compose([ToTensor(), Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
        transform_gt = Compose([ToTensor()])
        haze = transform_haze(haze_crop_img)
        gt = transform_gt(gt_crop_img)

        return haze, gt, haze_name

    def __getitem__(self, index):
        res = self.get_images(index)
        return res

    def __len__(self):
        return len(self.haze_names)

