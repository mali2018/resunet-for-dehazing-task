import time
import torch
import argparse
import torch.nn as nn
import torch.nn.functional as F
import matplotlib.pyplot as plt
from torch.utils.data import DataLoader
from train_data import TrainData
from val_data import ValData
from model import ResUnet
from utils import to_psnr, print_log, validation, adjust_learning_rate
from torchvision.models import vgg16
from perceptual import LossNetwork, LogCoshLoss
from torch.utils.tensorboard import SummaryWriter
plt.switch_backend('agg')


# --- Parse hyper-parameters  --- #
parser = argparse.ArgumentParser(description='Hyper-parameters for ResUNet')
parser.add_argument('--learning_rate', help='Set the learning rate', default=1e-3, type=float)
parser.add_argument('--crop_size', help='Set the crop_size', default=[240, 240], nargs='+', type=int)
parser.add_argument('--train_batch_size', help='Set the training batch size', default=4, type=int)

parser.add_argument('--lambda_loss', help='Set the lambda in loss function', default=0.04, type=float)
parser.add_argument('--val_batch_size', help='Set the validation/test batch size', default=1, type=int)
parser.add_argument('--category', help='Set image category (indoor or outdoor?)', default='indoor', type=str)
parser.add_argument('--model_dir', help='directory wherer save model paameter', default='dec_save', type=str)
args = parser.parse_args()

learning_rate = args.learning_rate
crop_size = args.crop_size
train_batch_size = args.train_batch_size
lambda_loss = args.lambda_loss
val_batch_size = args.val_batch_size
category = args.category
model_dir = args.model_dir

print('--- Hyper-parameters for training ---')
print('learning_rate: {}\ncrop_size: {}\ntrain_batch_size: {}\nval_batch_size: {}\nlambda_loss: {}\ncategory: {}'.format(learning_rate, crop_size,
      train_batch_size, val_batch_size,  lambda_loss, category))


if category == 'indoor':
    num_epochs = 100
    train_data_dir = '/home/chen009/Desktop/ResUnet/data/RESIDE_standard/ITS/'
    # val_data_dir = './data/test/SOTS/indoor/'
    val_data_dir = '/home/chen009/Desktop/ResUnetv0.2/data/test/SOTS/indoor/'
elif category == 'outdoor':
    num_epochs = 10
    train_data_dir = '/media/chen009/ExtraHardDisk2TB/Dataset/RESIDE_V0/OTS/'
    # val_data_dir = './data/test/SOTS/outdoor/'
    val_data_dir = '/home/chen009/Desktop/ResUnetv0.2/data/test/SOTS/outdoor/'
else:
    raise Exception('Wrong image category. Set it to indoor or outdoor for RESIDE dateset.')


device = torch.device("cuda: 0" if torch.cuda.is_available() else 'cpu')

net = ResUnet()
optimizer = torch.optim.Adam(net.parameters(), lr=learning_rate)
net = net.to(device)

# perceptual loss
vgg_model = vgg16(pretrained=True).features[:16]
vgg_model = vgg_model.to(device)
for param in vgg_model.parameters():
    param.requires_grad = False

loss_network = LossNetwork(vgg_model)
loss_network.eval()


try:
    net.dec.load_state_dict(torch.load(model_dir))
    print('-- weight loaded --')
except:
    print('--no weight loaded --')

pytorch_total_params = sum(p.numel() for p in net.parameters() if p.requires_grad)
print("Total_params: {}".format(pytorch_total_params))

# dataset = TrainData(crop_size,train_data_dir)
# hz,gt = dataset[0]
# print(hz.size())
# print(gt.size())
train_data_loader = DataLoader(TrainData(crop_size, train_data_dir), batch_size=train_batch_size, shuffle=True, num_workers=4)
val_data_loader = DataLoader(ValData(crop_size, val_data_dir), batch_size=val_batch_size, shuffle=False, num_workers=4)



# previous PSNR and SSIM in testing
old_val_psnr, old_val_ssim = validation(net, val_data_loader, device, category)
print('old_val_psnr: {0:.2f}, old_val_ssim: {1:.4f}'.format(old_val_psnr, old_val_ssim))
writer = SummaryWriter()
for epoch in range(num_epochs):
    psnr_list = []
    start_time = time.time()
    adjust_learning_rate(optimizer, epoch, category=category)

    for batch_id, train_data in enumerate(train_data_loader):

        haze, gt = train_data
        # print('haze',haze.size())
        haze = haze.to(device)
        gt = gt.to(device)

        optimizer.zero_grad()

        net.train()
        dehaze = net(haze)

        logcosh_loss =LogCoshLoss()(dehaze, gt)
        perceptual_loss = loss_network(dehaze, gt)
        loss = logcosh_loss + lambda_loss * perceptual_loss

        loss.backward()
        optimizer.step()

        psnr_list.extend(to_psnr(dehaze,gt))
        if not (batch_id % 100):
            print('Epoch: {0}, Iteration: {1}, loss:{2}'.format(epoch, batch_id, loss))
        writer.add_scalar('Loss/train', loss, epoch)

    # calculate the average training psnr in one epoch
    train_psnr = sum(psnr_list) / len(psnr_list)

    # save network parameters
    torch.save(net.dec.state_dict(),'{}'.format(category))

    net.eval()

    val_psnr, val_ssim = validation(net, val_data_loader, device, category)
    one_epoch_time = time.time() - start_time
    print_log(epoch + 1, num_epochs, one_epoch_time, train_psnr, val_psnr, val_ssim, category)


    if val_psnr >=old_val_psnr:
        # old_val_psnr = val_psnr
        torch.save(net.dec.state_dict(), '{}'.format(model_dir))
        old_val_psnr = val_psnr