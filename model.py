import torch
import torch.nn as nn

import torch.backends.cudnn as cudnn

import torchvision.models as models

import numpy as np

import os
import time
from datetime import datetime
import shutil

from collections import namedtuple


class VggEncoder(nn.Module):
    # torch.Size([4, 3, 620, 460]) 224
    # torch.Size([4, 64, 620, 460]) 224 relu1_1
    # torch.Size([4, 128, 310, 230]) 112    relu2_1
    # torch.Size([4, 256, 155, 115]) 56     relu3_1
    # torch.Size([4, 512, 77, 57]) 28       out
    def __init__(self, requires_grad=False):
        super(VggEncoder,self).__init__()
        self.encoder = nn.Sequential()

        vgg_pretrained_features = models.vgg19(pretrained=True).features
        for x in range(4):
            self.encoder.add_module(str(x), vgg_pretrained_features[x])

        self.encoder.add_module(str(4), nn.MaxPool2d(2, 2, return_indices=True))

        for x in range(5,9):
            self.encoder.add_module(str(x), vgg_pretrained_features[x])

        self.encoder.add_module(str(9), nn.MaxPool2d(2, 2, return_indices=True))

        for x in range(10, 18):
            self.encoder.add_module(str(x), vgg_pretrained_features[x])

        self.encoder.add_module(str(18), nn.MaxPool2d(2, 2, return_indices=True))

        for x in range(19, 21):
            self.encoder.add_module(str(x), vgg_pretrained_features[x])

        if not requires_grad:
            for param in self.parameters():
                param.requires_grad = False


    def forward(self, X):
        h = self.encoder[0](X)
        h = self.encoder[1](h)
        relu1_1 = h
        for i in range(2, 4):
            h=self.encoder[i](h)
        h, idx1 = self.encoder[4](h)
        pool1 = h

        h = self.encoder[5](h)
        h = self.encoder[6](h)
        relu2_1 = h
        for i in range(7, 9):
            h = self.encoder[i](h)
        h, idx2 = self.encoder[9](h)
        pool2 = h

        h = self.encoder[10](h)
        h = self.encoder[11](h)
        relu3_1 = h
        for i in range(12,18):
            h = self.encoder[i](h)
        h, idx3 = self.encoder[18](h)
        pool3 = h

        h = self.encoder[19](h)
        out = self.encoder[20](h)
        relu4_1 = h

        encoder_outputs = namedtuple("EncoderOutputs",['relu1_1', 'relu2_1', 'relu3_1', 'out'])
        outputs = encoder_outputs(relu1_1, relu2_1, relu3_1, out)

        return outputs


class Decoder(nn.Module):
    """
    use_sgm: last activation
    pretrained_dec: path of pretrained_decoder
    """
    def __init__(self, use_sgm='sigmoid'):
        super(Decoder, self).__init__()
        # self.pretraiend_dec = pretrained_dec
        self.reverseResNet = nn.Sequential(
            # block 1
            # input: 1024 x 77 x 57
            # 0
            nn.Conv2d(1024, 512, 3, 1, 1),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(512),

            # 512 x 77 x 57
            # 3
            nn.Conv2d(512, 512, 3, 1, 1),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(512),

            nn.Conv2d(512, 512, 3, 1, 1),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(512),

            # 256 x 77 x 57
            # 9
            nn.Conv2d(512, 256, 3, 1, 1),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(256),

            # 12
            nn.Conv2d(256, 256, 3, 1, 1),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(256),

            nn.Conv2d(256, 256, 3, 1, 1),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(256),
            # out = (in - 1) x stride(1) - 2 x padding(0) +
            #       dilation(1) x (kernel_size - 1) + output_padding(0) + 1
            # 256 x 155 x 115
            # 18
            nn.ConvTranspose2d(256, 256, kernel_size=4, stride=2, padding=1), # 2 x in + 1

            # block 2
            # input: 512 x 155 x 115
            # 19
            nn.Conv2d(512, 256, 3, 1, 1),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(256),

            # 256 x 155 x 115
            # 22
            nn.Conv2d(256, 256, 3, 1, 1),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(256),

            # 25
            nn.Conv2d(256, 256, 3, 1, 1),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(256),

            # 128 x 155 x 115
            # 28
            nn.Conv2d(256, 128, 3, 1, 1),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(128),

            # 31
            nn.Conv2d(128, 128, 3, 1, 1),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(128),

            # 34
            nn.Conv2d(128, 128, 3, 1, 1),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(128),
            # 128 x 310 x 230
            # 37
            nn.ConvTranspose2d(128, 128, 4, 2, 1),

            # block 3
            # input: 256 x 310 x 230
            # 38
            nn.Conv2d(256, 64, 3, 1, 1),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(64),

            # 64 x 310 x 230
            # 41
            nn.Conv2d(64, 64, 3, 1, 1),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(64),

            # 44
            nn.Conv2d(64, 64, 3, 1, 1),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(64),

            # 64 x 620 x 460
            # 47
            nn.ConvTranspose2d(64, 64, 4, 2, 1),

            # last block(output block without last activation)
            # 48
            nn.Conv2d(128, 3, 1, 1, 0),


        )

        self.use_sgm = use_sgm
        # last activation
        if self.use_sgm == 'sigmoid':
            self.lastactivation = nn.Sigmoid()
        elif self.use_sgm == 'tanh':
            self.lastactivation = nn.Tanh()
        elif self.use_sgm.lower() != 'none':
            print('unknow last decoder layer flag:', self.use_sgm)

        self.norm4 = nn.GroupNorm(32, 512, affine=True)
        self.norm3 = nn.GroupNorm(32, 256, affine=True)
        self.norm2 = nn.GroupNorm(32, 128, affine=True)
        self.norm1 = nn.GroupNorm(32, 64, affine=True)

        # self.conv1_4 = nn.Conv2d(512, 256, 1, 2, 0)
        # self.conv1_3 = nn.Conv2d(256, 128, 1, 2, 0)
        # self.conv1_2 = nn.Conv2d(128, 56, 1, 2, 0)


    def forward(self, X):
        input_d_in = self.norm4(X.out)
        input_relu3_1_in = self.norm3(X.relu3_1)
        input_relu2_1_in = self.norm2(X.relu2_1)
        input_relu1_1_in = self.norm1(X.relu1_1)

        # if self.pretraiend_dec is not None:
        #     self.load_dec()
        out = torch.cat((X.out, input_d_in),dim=1)

        # block 1
        for idx in range(0, 3):
            out = self.reverseResNet[idx](out)
        identity1 = out
        for idx in range(3, 9):
            out = self.reverseResNet[idx](out)
        out += identity1
        for idx in range(9, 12):
            out = self.reverseResNet[idx](out)
        identity2 = out
        for idx in range(12, 18):
            out = self.reverseResNet[idx](out)
        out += identity2
        out = self.reverseResNet[18](out)

        out = torch.cat((out, input_relu3_1_in), dim=1)

        # block 2
        for idx in range(19, 22):
            out = self.reverseResNet[idx](out)
        identity3 = out
        for idx in range(22, 28):
            out = self.reverseResNet[idx](out)
        out += identity3
        for idx in range(28, 31):
            out = self.reverseResNet[idx](out)
        identity4 = out
        for idx in range(31, 37):
            out = self.reverseResNet[idx](out)
        out += identity4
        out = self.reverseResNet[37](out)

        out = torch.cat((out, input_relu2_1_in), dim=1)

        # block 3
        for idx in range(38, 41):
            out = self.reverseResNet[idx](out)
        identity5 = out
        for idx in range(41, 47):
            out = self.reverseResNet[idx](out)
        out += identity5
        out = self.reverseResNet[47](out)

        out = torch.cat((out, input_relu1_1_in), dim=1)

        # last block
        out = self.reverseResNet[48](out)
        out = self.lastactivation(out)

        # # last activation
        # if self.use_sgm == 'sigmoid':
        #     out = nn.Sigmoid()(out)
        # elif self.use_sgm == 'tanh':
        #     out = use_sgm = nn.Tanh()(out)
        # elif self.use_sgm.lower() != 'none':
        #     print('unknow last decoder layer flag:', self.use_sgm)

        return out


    # def load_dec(self):
    #     self.reverseResNet.load_state_dict(torch.load(self.pretraiend_dec))


# ------ Main model ------ #
class ResUnet(nn.Module):
    def __init__(self):
        super(ResUnet, self).__init__()
        self.enc = VggEncoder(requires_grad=False)

        self.dec = Decoder(use_sgm='sigmoid') #,pretrained_dec=self.pretrained_dec)

    def forward(self, X):
        out = self.enc(X)
        out = self.dec(out)
        return out


# rmat = torch.randn(4,3,240,240)
# en=VggEncoder()(rmat)
#
# res = ResUnet()(rmat)
# print(en.relu1_1.size())
# print(en.relu2_1.size())
# print(en.relu3_1.size())
# print(en.out.size())
#
# decoder= Decoder()(en)
