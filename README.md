Guidence:


**Usage:**
* 1. cd to this position where you download our project.
* 2.  create and place your haze image into */real_haze image* folder which should be saved under this project.
* 3. use `python3 gen.py` to generate dehazed images with our pretrained model, named *dec_save*.

For training model on you data set, we are pity to tell you that you have to change *train.py*, *train_data.py* and *val_data.py* appropritely.


**environment:**
* python:3.7.3
* pytorch:1.2.0+
* cudatoolkit:10.1


you can download *dec_save* model from [dec_save(old)](https://drive.google.com/open?id=1UTzDQHGXRyCB8zeTjxtVqenbIdXPN0PZ) or [dec_save(recent)](https://drive.google.com/file/d/14-4uOsA_2yQLy_vaK16GVwS0p2KkTTez/view?usp=sharing).


We cite part of code from [GridDehazeNet.](https://github.com/proteus1991/GridDehazeNet) and use same data structure.

PSNR and SSIM in different methods:

| Methods | DCP | Haze line | AOD-Net | MSCNN | Ours(GN) |
| ------ | ------ | ------ | ------ | ------ | ------ |
| PSNR| 16.32 | 25.96 | 19.64 | 17.43 | 24.83 |
| SSIM | 0.8472 | 0.9362 | 0.8453 | 0.7534 | 0.9207 |  

![image](https://gitlab.com/mali2018/resunet-for-dehazing-task/raw/master/results.png)


time cost:
![imge](https://gitlab.com/mali2018/resunet-for-dehazing-task/raw/master/time_cost.png)